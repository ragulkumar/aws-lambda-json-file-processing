import unittest
import pytest
from mock import patch
import datetime

from src.s3_to_dynamodb import fetch_s3object, upload_dynamodb

event = {
  "Records": [
    {
      "eventVersion": "2.0",
      "eventSource": "aws:s3",
      "awsRegion": "us-east-1",
      "eventTime": "1970-01-01T00:00:00.000Z",
      "eventName": "ObjectCreated:Put",
      "userIdentity": {
        "principalId": "EXAMPLE"
      },
      "requestParameters": {
        "sourceIPAddress": "127.0.0.1"
      },
      "responseElements": {
        "x-amz-request-id": "EXAMPLE123456789",
        "x-amz-id-2": "EXAMPLE123/5678abcdefghijklambdaisawesome/mnopqrstuvwxyzABCDEFGH"
      },
      "s3": {
        "s3SchemaVersion": "1.0",
        "configurationId": "testConfigRule",
        "bucket": {
          "name": "example-bucket",
          "ownerIdentity": {
            "principalId": "EXAMPLE"
          },
          "arn": "arn:aws:s3:::example-bucket"
        },
        "object": {
          "key": "test%2Fkey",
          "size": 1024,
          "eTag": "0123456789abcdef0123456789abcdef",
          "sequencer": "0A1B2C3D4E5F678901"
        }
      }
    }
  ]
}
data = {"id": "2", "profile": "Java", "email": "abc@deloitte.com"}

class Boto3Client:
  def __init__(self,service):
    self.service = service

  def get_object(self,**kvargs):
    response = {'ResponseMetadata': {'RequestId': '30FS31X96H7P5BCG', 'HostId': 'Aou7hjxPvqSBDTH5IxBRWVR2G7hP7SbFIDNNrQTgeyd5GFyCITWs9rvGaSeVvIoqOA1rszPHbw8=', 'HTTPStatusCode': 200, 'HTTPHeaders': {'x-amz-id-2': 'Aou7hjxPvqSBDTH5IxBRWVR2G7hP7SbFIDNNrQTgeyd5GFyCITWs9rvGaSeVvIoqOA1rszPHbw8=', 'x-amz-request-id': '30FS31X96H7P5BCG', 'date': 'Tue, 19 Apr 2022 10:20:57 GMT', 'last-modified': 'Mon, 18 Apr 2022 17:27:02 GMT', 'etag': '"c5eb9fae09cbdba76c2327b87c6835c1"', 'accept-ranges': 'bytes', 'content-type': 'binary/octet-stream', 'server': 'AmazonS3', 'content-length': '59'}, 'RetryAttempts': 0}, 'AcceptRanges': 'bytes', 'ContentLength': 59, 'ETag': '"c5eb9fae09cbdba76c2327b87c6835c1"', 'ContentType': 'binary/octet-stream', 'Metadata': {}, 'Body': Body()}
    return response


class Body:
  def read(self):
    return b'{"id": "1", "profile": "Java", "email": "abc@deloitte.com"}'


class Boto3Resource:
  def __init__(self,service):
    self.service = service
  def Table(self,table):
    return DynamoObjects()


class DynamoObjects:
  def put_item(self, **kvargs):
    response = {'ResponseMetadata': {'RequestId': 'RUSBI3P9K4C18NHJQ7D09HEQFRVV4KQNSO5AEMVJF66Q9ASUAAJG', 'HTTPStatusCode': 200, 'HTTPHeaders': {'server': 'Server', 'date': 'Tue, 19 Apr 2022 08:15:47 GMT', 'content-type': 'application/x-amz-json-1.0', 'content-length': '2', 'connection': 'keep-alive', 'x-amzn-requestid': 'RUSBI3P9K4C18NHJQ7D09HEQFRVV4KQNSO5AEMVJF66Q9ASUAAJG', 'x-amz-crc32': '2745614147'}, 'RetryAttempts': 0}}
    return response

#mock the boto3.client package
@patch('boto3.client', Boto3Client)
def test_fetch_s3object():
  response = fetch_s3object(event)
  assert type(response) is dict

#mock the boto3.resource package
@patch('boto3.resource', Boto3Resource)
def test_upload_dynamodb():
  response = upload_dynamodb(data)
  assert response['ResponseMetadata']['HTTPStatusCode'] == 200

if __name__ == '__main__':
  unittest.main()
