# This is a script to reads the JSON object from S3 and uploads the data to DynamoDB table
import json
import boto3
import datetime
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

#uploads the JSON item to dynamodb table
def upload_dynamodb(data):
    try:
        dynamo_obj = boto3.resource('dynamodb').Table('profiles')
        return dynamo_obj.put_item(Item=data)
    except Exception as err:
        logging.error('Error in uploading files to DynamoDB table - '+ str(err))
        raise Exception(err)

#On S3 trigger, get the bucket and key details from S3 event and reads the JSON object from S3
def fetch_s3object(event):
    try:
        s3_obj = boto3.client('s3')
        bucket_name = event['Records'][0]['s3']['bucket']['name']
        key_name = event['Records'][0]['s3']['object']['key']
        object = s3_obj.get_object(Bucket=bucket_name, Key=key_name)
        print(object)
        json_content = object['Body'].read().decode("utf-8")
        data = json.loads(json_content)
        data['processed_time'] = str(datetime.datetime.now())
        logging.info('Successfully fetch object from S3 bucket')
        return data
    except Exception as err:
        logging.error('Error in reading files from s3 bucket - '+ str(err))
        raise Exception(err)

def driver(event,context = None):
    data = fetch_s3object(event)
    print(data)
    response = upload_dynamodb(data)
    if response['ResponseMetadata']['HTTPStatusCode'] == 200:
        logging.info('Successfully uploads item to DynamoDB table')

if __name__=='__main__':
    event = {"Records": [
    {
      "s3": {
        "bucket": {"name": "test-file-container"},
        "object": { "key": "Java/6.json"}
          }
        }
      ]
    }
    driver(event)